using System;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Controls.Documents;
using Avalonia.Media;
using Avalonia.Threading;

namespace CMac2Port.Controls;

public partial class LogConsole : UserControl
{
    public static LogConsole Instance { get; private set; } = null!;

    public enum LogColor
    {
        Default = 0,
        Green = 1,
        Red = 2
    }

    private readonly SolidColorBrush[] _colors =
    [
        new(Colors.WhiteSmoke),
        new(Colors.LimeGreen),
        new(Colors.OrangeRed)
    ];

    public LogConsole()
    {
        InitializeComponent();
        Instance = this;
    }

    public async Task Log(string format, params object[] args)
    {
        await Log(LogColor.Default, format, args);
    }

    public async Task LogSuccess(string format, params object[] args)
    {
        await Log(LogColor.Green, format, args);
    }

    public async Task LogError(string format, params object[] args)
    {
        await Log(LogColor.Red, format, args);
    }

    private async Task Log(LogColor color, string format, params object[] args)
    {
        var msg = args.Length > 0 ? string.Format(format, args) : format;
        Console.WriteLine(msg);

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            TextBlockLog!.Inlines!.Add
            (
                new Run
                {
                    Text = "\n" + msg,
                    Foreground = _colors[(int)color],
                }
            );

            ScrollViewerLog!.ScrollToEnd();
        });
    }

    public async Task Clear()
    {
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            TextBlockLog!.Inlines!.Clear();
            TextBlockLog!.Text = string.Empty;
        });
    }
}