using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Media;
using Avalonia.Platform.Storage;
using Avalonia.Styling;
using CMac2Port.Controls;
using CMac2Port.Sources;
using Material.Icons;

namespace CMac2Port;

public partial class MainWindow : Window
{
    public static bool IsWindows => OperatingSystem.IsWindows();

    private readonly Preferences _prefs = new();
    private Csv _csv = new();
    private bool _stopped = true;

    public MainWindow()
    {
        InitializeComponent();
        Loaded += async (_, _) => await InitializeAsync();
    }

    private async Task InitializeAsync()
    {
        // load preferences (rpe address, username, ...)
        await _prefs.Load();

        // update text boxes
        TextBoxAddress.Text = _prefs.Host;
        TextBoxUser.Text = _prefs.Username;
        TextBoxPwd.Text = _prefs.Password;
        TextBoxMac.Text = _prefs.Mac;
        CheckBoxUseSsh.IsChecked = _prefs.UseSsh;

        if (Design.IsDesignMode)
        {
            for (var i = 0; i < 10; i++)
            {
                _csv.Computers.Add(new Computer
                {
                    Name = "B120P01",
                    Mac = "00:00:00:00:00:00",
                    Switch = "Switch Test 1"
                });
            }
        }

        ComputerGrid.ItemsSource = _csv.Computers;
    }

    private async void OnButtonSearch(object? sender, RoutedEventArgs e)
    {
        // remove unused parameter warning
        _ = sender;
        _ = e;

        // clear logs
        await LogConsole.Instance.Clear();

        // disable fields input
        EnableInputs(false);

        // disable export button if any
        ButtonExport.IsVisible = false;

        if (string.IsNullOrEmpty(TextBoxAddress.Text)
            || string.IsNullOrEmpty(TextBoxUser.Text)
            || string.IsNullOrEmpty(TextBoxPwd.Text)
            || string.IsNullOrEmpty(TextBoxMac.Text))
        {
            await LogConsole.Instance.LogError("a value is missing, please check your inputs...");
            // enable fields input back
            EnableInputs(true);
            return;
        }

        // update/save preferences
        await UpdatePrefs();

        // let's go...
        var computer = await Task.Run(() => SearchMac(_prefs.Host, _prefs.UseSsh ? "22" : "23",
            _prefs.Username, _prefs.Password, new Computer { Mac = _prefs.Mac }));
        if (computer != null)
        {
            if (_csv.Computers.FirstOrDefault(c => c.Mac == computer.Mac) == null)
            {
                _csv.Computers.Add(computer);
            }
        }

        // enable fields input back
        EnableInputs(true);
    }

    private async void OnButtonLoadCsv(object? sender, RoutedEventArgs e)
    {
        // remove unused parameter warning
        _ = sender;
        _ = e;

        // get top level from the current control
        var topLevel = GetTopLevel(this);

        // open the file picker
        var files = await topLevel!.StorageProvider.OpenFilePickerAsync(new FilePickerOpenOptions
        {
            Title = "Choose a GLPI export (CSV)",
            AllowMultiple = false,
            FileTypeFilter =
            [
                new FilePickerFileType("GLPI export") { Patterns = ["*.csv"], MimeTypes = ["text/csv"] },
                FilePickerFileTypes.TextPlain
            ]
        });

        // nothing to do...
        if (files.Count < 1) return;

        // enable "start" button
        EnableInputs(false);

        // load csv file
        _csv = await Csv.Load(files[0].Path.AbsolutePath);
        if (_csv.Computers.Count < 1)
        {
            await LogConsole.Instance.LogError("malformed or empty csv file, please check your inputs...");
            ButtonStartStop.IsEnabled = false;
            return;
        }

        // update computer grid
        ComputerGrid.ItemsSource = _csv.Computers;

        // enable "start" button
        EnableInputs(true);
    }

    private async void OnButtonStartStop(object? sender, RoutedEventArgs e)
    {
        // remove unused parameter warning
        _ = sender;
        _ = e;

        if (string.IsNullOrEmpty(TextBoxAddress.Text)
            || string.IsNullOrEmpty(TextBoxUser.Text)
            || string.IsNullOrEmpty(TextBoxPwd.Text))
        {
            await LogConsole.Instance.LogError("a value is missing, please check your inputs...");
            return;
        }

        // update/save preferences
        await UpdatePrefs();

        if (_stopped)
        {
            await Search(_csv.Computers);
        }
        else
        {
            _stopped = true;
            await LogConsole.Instance.Log("stopping...");
        }
    }

    private async Task Search(ObservableCollection<Computer> computers)
    {
        if (computers.Count < 1)
        {
            await LogConsole.Instance.LogError("oops: no computer found to scan (empty csv file)...");
            return;
        }

        // set "stopped" status
        _stopped = false;

        // clear logs
        await LogConsole.Instance.Clear();

        // disable fields input
        EnableInputs(false);

        // disable export button if any
        ButtonExport.IsVisible = false;

        // set DataGrid highlight
        SetHighlight(true);

        foreach (var computer in computers)
        {
            if (_stopped) break;

            // set selected computer grid item to current computer
            ComputerGrid.SelectedItem = computer;
            ComputerGrid.ScrollIntoView(computer, null);

            await LogConsole.Instance.Log($"searching for {computer.Mac} ({computer.Name})");
            var c = await Task.Run(() => SearchMac(_prefs.Host, _prefs.UseSsh ? "22" : "23",
                _prefs.Username, _prefs.Password, computer));
            if (c is null) continue;

            // update changed properties only
            computer.Update(c);
        }

        // restore DataGrid "highlight"
        SetHighlight(false);

        // enable fields input back
        EnableInputs(true);

        // enable export button if found mac > 0
        ButtonExport.IsVisible = _csv.Computers.Count > 0;

        _stopped = true;
    }

    private async void OnButtonExport(object? sender, RoutedEventArgs e)
    {
        // remove unused parameter warning
        _ = sender;
        _ = e;

        // get top level from the current control. Alternatively, you can use Window reference instead.
        var topLevel = GetTopLevel(this);

        // start async operation to open the dialog.
        var file = await topLevel!.StorageProvider.SaveFilePickerAsync(new FilePickerSaveOptions
        {
            Title = "Save csv file"
        });

        if (file is null) return;

        if (!await Csv.Save(file.Path.AbsolutePath, _csv))
        {
            await LogConsole.Instance.LogError("oops: can't open file for writing...");
            return;
        }

        await LogConsole.Instance.LogSuccess("csv file exported successfully...");
    }

    private async Task<Computer?> SearchMac(string host, string port, string user, string pwd, Computer computer)
    {
        await LogConsole.Instance.Log($"connecting to {host}:{port}");
        var device = new Device(_prefs.UseSsh
            ? CiscoClient.Proto.Ssh
            : CiscoClient.Proto.Telnet, host, user, pwd);
        var res = await device.Connect();
        if (res != null)
        {
            await LogConsole.Instance.LogError("connection failed: " + res);
            device.Dispose();
            return null;
        }

        await LogConsole.Instance.Log("connected to " + device.Address + ":" + device.Port + " (" + device.Name + ")");
        await LogConsole.Instance.Log($"searching for {computer.Mac} ({computer.Name})");
        var address = await device.GetMacAddress(computer.Mac);
        if (address == null)
        {
            await LogConsole.Instance.LogError("mac not found on " + device.Name +
                                               ", is the device up and running? ");
            device.Dispose();
            return null;
        }

        // check if interface belong to a port channel (stacked)
        await LogConsole.Instance.Log("looking if mac address interface is on a port channel interface...");
        if (address.Port.StartsWith("Port-channel"))
        {
            await LogConsole.Instance.Log(
                "mac address interface belong to a port channel, getting port channel interfaces...");
            var interfaces = await device.GetPortChannelInterfaces(address.Port);
            if (interfaces is { Count: > 0 }) address.Port = interfaces[0];
        }

        // mac address found, check if it belongs to a neighbor
        var neighbor = await device.GetNeighbor(address.Port);
        if (neighbor != null)
        {
            //await Log("mac found on neighbor: " + neighbor + ", processing....");
            await LogConsole.Instance.Log("mac found on " + neighbor.Interface + " connected to " + neighbor.DeviceId);
            device.Dispose();
            return await SearchMac(neighbor.Address, port, user, pwd, computer);
        }

        await LogConsole.Instance.LogSuccess(
            "mac address found on " + device.Name + " (" + device.Address + ")" + ": "
            + address.Port + ", vlan " + address.Vlan);
        computer.Switch = device.Name;
        computer.SwitchIp = device.Address;
        computer.Interface = address.Port;
        computer.Vlan = address.Vlan;

        device.Dispose();
        return computer;
    }

    private async Task UpdatePrefs()
    {
        // update/save preferences
        _prefs.Host = TextBoxAddress.Text!;
        _prefs.Username = TextBoxUser.Text!;
        _prefs.Password = TextBoxPwd.Text!;
        _prefs.Mac = TextBoxMac.Text!;
        if (CheckBoxUseSsh.IsChecked != null) _prefs.UseSsh = CheckBoxUseSsh.IsChecked.Value;
        await _prefs.Save();
    }

    private void EnableInputs(bool enable)
    {
        TextBoxAddress.IsEnabled = TextBoxUser.IsEnabled = TextBoxPwd.IsEnabled
            = TextBoxMac.IsEnabled = ButtonSearch.IsEnabled = CheckBoxUseSsh.IsEnabled
                = ButtonLoadCsv.IsEnabled = ComputerGrid.IsEnabled = enable;
        ButtonStartStop.IsVisible = _csv.Computers.Count > 0;
        ButtonStartStop.Foreground = enable ? Brushes.LimeGreen : Brushes.Crimson;
        ButtonStartStopIcon.Kind = enable ? MaterialIconKind.Play : MaterialIconKind.Stop;
        ToolTip.SetTip(ButtonStartStop, enable ? "Search all mac addresses" : "Stop this crap !");
    }

    private void SetHighlight(bool value)
    {
        var style = new Style(x => x.OfType<DataGridRow>().Class(":selected"))
        {
            Setters =
            {
                new Setter(FontWeightProperty, value ? FontWeight.Bold : FontWeight.Normal)
            }
        };

        ComputerGrid.Styles.Add(style);
    }

    private async void OnSearchComputer(object? sender, RoutedEventArgs e)
    {
        var computers = new ObservableCollection<Computer>(ComputerGrid.SelectedItems.Cast<Computer>());
        await Search(computers);
    }

    /*
    private void OnDeleteComputer(object? sender, RoutedEventArgs e)
    {
        throw new System.NotImplementedException();
    }
    */
}