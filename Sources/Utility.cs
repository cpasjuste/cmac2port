using System;
using System.Net.NetworkInformation;

namespace CMac2Port.Sources;

public abstract class Utility
{
    public static bool IsUp(string host, int timeout = 2000)
    {
        try
        {
            var ping = new Ping();
            return ping.Send(host, timeout).Status == IPStatus.Success;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public static Neighbor? FindNeighbor(string reply, string @interface)
    {
        if (!reply.Contains("Interface: " + @interface + ",")) return null;

        var sections = reply.Split("-------------------------");
        foreach (var section in sections)
        {
            if (!section.Contains("Interface: " + @interface + ",")) continue;

            var neighbor = new Neighbor();
            // parse device id
            var start = section.IndexOf("Device ID: ", StringComparison.Ordinal) + "Device ID: ".Length;
            var end = section.IndexOf("\r\n", start, StringComparison.Ordinal);
            neighbor.DeviceId = section.Substring(start, end - start);
            // parse address
            start = section.IndexOf("IP address: ", StringComparison.Ordinal) + "IP address: ".Length;
            end = section.IndexOf("\r\n", start, StringComparison.Ordinal);
            neighbor.Address = section.Substring(start, end - start);
            // parse Interface
            start = section.IndexOf("Interface: ", StringComparison.Ordinal) + "Interface: ".Length;
            end = section.IndexOf(",", start, StringComparison.Ordinal);
            neighbor.Interface = section.Substring(start, end - start);
            return neighbor;
        }

        return null;
    }
}