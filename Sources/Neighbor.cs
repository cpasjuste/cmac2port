namespace CMac2Port.Sources;

public class Neighbor
{
    public string DeviceId { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;

    public string Interface { get; set; } = string.Empty;

    public Neighbor()
    {
    }

    public Neighbor(string device, string address, string @interface)
    {
        DeviceId = device;
        Address = address;
        Interface = @interface;
    }

    public override string ToString()
    {
        return "DeviceId: " + DeviceId + ", Address: " + Address + ", Interface: " + Interface;
    }
}