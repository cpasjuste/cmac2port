using System;
using System.Threading;
using System.Threading.Tasks;
using CMac2Port.Controls;
using PrimS.Telnet;

namespace CMac2Port.Sources;

public class CiscoClientTelnet(string host, int port, string user, string password)
    : CiscoClient(Proto.Telnet, host, port, user, password)
{
    private Client? _client;

    public override async Task<string?> Connect()
    {
        await LogConsole.Instance.Log("checking if host is up ({0})...", Address);
        if (!Utility.IsUp(Address))
        {
            await LogConsole.Instance.LogError("could not connect to {0} on port {1}", Address, Port);
            return "host not reachable...";
        }

        try
        {
            //await LogConsole.Instance.Log("connecting to device ({0})", Address);
            _client = new Client(new TcpByteStream(Address, Port),
                new TimeSpan(0, 0, 5), new CancellationToken());
        }
        catch (Exception e)
        {
            await LogConsole.Instance.Log(e.Message);
            return e.Message;
        }

        if (!_client.IsConnected)
        {
            await LogConsole.Instance.LogError("could not connect to {0} on port {1}", Address, Port);
            return "unknown error...";
        }

        // log-in
        await LogConsole.Instance.Log("logging in (user: {0}, password: xxx)...", Username);
        var res = await _client.TryLoginAsync(Username, Password, 5000, "#", "\n");
        if (!res)
        {
            await LogConsole.Instance.LogError("login failed...");
            return "login failed...";
        }

        // be sure we get full output
        await Send("terminal length 0");

        return null;
    }

    public override bool IsConnected => _client is { IsConnected: true };

    public override async Task<string?> Send(string cmd)
    {
        if (_client == null) return null;

        try
        {
            // send command
            await _client.WriteLineAsync(cmd);
            // retrieve reply
            var reply = await _client.TerminatedReadAsync("#", TimeSpan.FromMilliseconds(5000));
            // cleanup reply (remove command "echo")...
            var start = reply.IndexOf("\r\n", StringComparison.Ordinal);
            if (start > 0) reply = reply.Substring(start + 2, reply.Length - (start + 2));
            // cleanup reply (remove prompt)...
            var end = reply.LastIndexOf("\r\n", StringComparison.Ordinal);
            if (end > 0) reply = reply.Substring(0, end);
            return reply.Trim();
        }
        catch (Exception e)
        {
            await LogConsole.Instance.Log(e.Message);
        }

        return null;
    }

    public override void Dispose()
    {
        if (_client is { IsConnected: true }) _client.WriteLineAsync("exit");
        _client?.Dispose();
        GC.SuppressFinalize(this);
    }
}