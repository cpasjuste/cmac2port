using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CMac2Port.Controls;

namespace CMac2Port.Sources;

public class Device : IDisposable
{
    public string Name => _client.Name;
    public string Address => _client.Address;
    public int Port => _client.Port;
    public string Username => _client.Username;
    public string Password => _client.Password;

    private readonly CiscoClient _client;

    public Device(CiscoClient.Proto proto, string host, string user, string password)
    {
        if (proto == CiscoClient.Proto.Ssh) _client = new CiscoClientSsh(host, 22, user, password);
        else _client = new CiscoClientTelnet(host, 23, user, password);
    }

    public async Task<string?> Connect()
    {
        var res = await _client.Connect();
        if (res != null) return res;

        // load hostname
        await LogConsole.Instance.Log("loading hostname...");
        if (!await LoadHostname())
        {
            await LogConsole.Instance.LogError("warning: could not retrieve hostname...");
        }

        // all done
        await LogConsole.Instance.Log("device loaded: {0}, address: {1}", Name, Address);

        return null;
    }

    public async Task<CiscoInterface?> GetMacAddress(string mac)
    {
        if (!_client.IsConnected) return null;

        try
        {
            // build and send command
            var address = mac.Replace(":", "").Replace("-", "")
                .Replace(".", "").ToLower();
            address = Regex.Replace(address, "(.{4})(.{4})(.{4})", "$1.$2.$3");

            var reply = await _client.Send("show mac address-table address " + address + " | i " + address);
            if (string.IsNullOrEmpty(reply)) return null;

            var split = Regex.Split(reply, @"\s{1,}");

            // Cisco IOS Software, Catalyst 4500 L3 Switch Software (cat4500-ENTSERVICESK9-M), Version 12.2(53)SG4, RELEASE SOFTWARE (fc2)
            if (split.Length == 5) return new CiscoInterface(split[0], split[1], split[4]);

            // Cisco IOS Software, C2960 Software (C2960-LANBASEK9-M), Version 12.2(44)SE6, RELEASE SOFTWARE (fc1)
            if (split.Length == 4)
            {
                // get interface full name, a little tricky...
                var str = await _client.Send("show interfaces " + split[3] + " | i line protocol");
                if (string.IsNullOrEmpty(str)) return null;
                return new CiscoInterface(split[0], split[1], str[..str.IndexOf(' ')]);
            }
        }
        catch (Exception e)
        {
            await LogConsole.Instance.LogError(e.Message);
        }

        return null;
    }

    public async Task<Neighbor?> GetNeighbor(string port)
    {
        if (!_client.IsConnected) return null;

        try
        {
            var reply = await _client.Send("show cdp neighbors detail");
            return string.IsNullOrEmpty(reply) ? null : Utility.FindNeighbor(reply, port);
        }
        catch (Exception e)
        {
            await LogConsole.Instance.LogError(e.Message);
        }

        return null;
    }

    public async Task<List<string>?> GetPortChannelInterfaces(string port)
    {
        if (!_client.IsConnected) return null;

        // extract port channel group from interface name
        port = port.Replace("Port-channel", "");

        try
        {
            var reply = await _client.Send("show etherchannel " + port + " port-channel");
            if (reply == null) return null;

            var ints = new List<string>();
            var lines = reply.Split("\n");
            foreach (var line in lines)
            {
                if (!line.Contains(" Active ") && !line.Contains(" On ")) continue;
                var split = Regex.Split(line.Trim(), @"\s{1,}");
                if (split.Length == 5)
                {
                    // get interface full name, a little tricky...
                    var str = await _client.Send("show interfaces " + split[2].Trim() + " | i line protocol");
                    if (string.IsNullOrEmpty(str)) continue;
                    ints.Add(str.Substring(0, str.IndexOf(" ", StringComparison.Ordinal)));
                }
            }

            return ints;
        }
        catch (Exception e)
        {
            await LogConsole.Instance.Log(e.Message);
            return null;
        }
    }

    private async Task<bool> LoadHostname()
    {
        if (!_client.IsConnected) return false;

        try
        {
            var reply = await _client.Send("show version | in uptime");
            if (reply == null) return false;
            var end = reply.IndexOf(" uptime", StringComparison.Ordinal);
            if (end <= 0) return false;
            _client.Name = reply.Substring(0, end);
        }
        catch (Exception e)
        {
            await LogConsole.Instance.Log(e.Message);
            return false;
        }

        return true;
    }

    public void Dispose()
    {
        _client.Dispose();
        GC.SuppressFinalize(this);
    }
}