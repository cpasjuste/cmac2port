﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using CMac2Port.Controls;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;

namespace CMac2Port.Sources;

public class Csv
{
    public ObservableCollection<Computer> Computers { get; set; } = [];

    public static async Task<Csv> Load(string path)
    {
        var csv = new Csv();
        using var reader = new StreamReader(path);
        var config = new CsvConfiguration(CultureInfo.InvariantCulture)
        {
            Delimiter = ";",
            BadDataFound = null,
            MissingFieldFound = null,
            HeaderValidated = null
        };

        try
        {
            using var csvReader = new CsvReader(reader, config);
            csvReader.Context.RegisterClassMap<ComputerMap>();
            await foreach (var computer in csvReader.GetRecordsAsync<Computer>())
            {
                csv.Computers.Add(computer);
            }
        }
        catch (Exception ex)
        {
            await LogConsole.Instance.LogError(ex.Message);
        }

        return csv;
    }

    public static async Task<bool> Save(string filePath, Csv csv)
    {
        var config = new CsvConfiguration(CultureInfo.InvariantCulture)
        {
            Delimiter = ";",
            HasHeaderRecord = true
        };

        try
        {
            await using var writer = new StreamWriter(filePath);
            await using var csvWriter = new CsvWriter(writer, config);
            csvWriter.Context.RegisterClassMap<ComputerMap>();
            await csvWriter.WriteRecordsAsync(csv.Computers);
        }
        catch (Exception ex)
        {
            await LogConsole.Instance.Log(ex.Message);
            return false;
        }

        return true;
    }

    private sealed class ComputerMap : ClassMap<Computer>
    {
        public ComputerMap()
        {
            Map(c => c.Name).Name("Name", "Nom");
            // glpi computers can have multiple mac addresses (ethernet/wifi/...), keep first one for now...
            Map(c => c.Mac).Name("Mac", "Composants - Adresse MAC").TypeConverter<MacAddressConverter>();
            Map(c => c.Switch).Name("Switch");
            Map(c => c.SwitchIp).Name("SwitchIp");
            Map(c => c.Interface).Name("Interface");
            Map(c => c.Vlan).Name("Vlan");
        }
    }

    private sealed class MacAddressConverter : ITypeConverter
    {
        public object ConvertFromString(string? text, IReaderRow row, MemberMapData memberMapData)
        {
            // check if the value contains "<br>" and take only the part before it
            if (!string.IsNullOrEmpty(text))
            {
                var parts = text.Split(["<br>"], StringSplitOptions.None);
                return parts[0].Trim(); // return the part before "<br>"
            }

            // return the original value if "<br>" is not found
            return text ?? string.Empty;
        }

        public string? ConvertToString(object? value, IWriterRow row, MemberMapData memberMapData)
        {
            return value?.ToString();
        }
    }
}