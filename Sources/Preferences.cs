using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization.Metadata;
using System.Threading.Tasks;
using CMac2Port.Controls;
using DeviceId;

namespace CMac2Port.Sources;

public class Preferences
{
    public string Host { get; set; } = string.Empty;

    public string Username { get; set; } = string.Empty;

    public string Password { get; set; } = string.Empty;
    public string Mac { get; set; } = string.Empty;

    public bool UseSsh { get; set; } = true;

    private readonly string _prefPath = Path.Combine(Environment.GetFolderPath(
        Environment.SpecialFolder.ApplicationData), "CMac2Port\\prefs.json");

    private string _uid = string.Empty;

    [System.Diagnostics.CodeAnalysis.UnconditionalSuppressMessage("Trimming", "IL2026")]
    private readonly JsonSerializerOptions _options = new()
    {
        TypeInfoResolver = new DefaultJsonTypeInfoResolver()
    };

    [System.Diagnostics.CodeAnalysis.UnconditionalSuppressMessage("Trimming", "IL2026")]
    public async Task Load()
    {
        try
        {
            // create unique encryption key
            _uid = new DeviceIdBuilder()
                .AddMachineName()
                .AddMacAddress()
                .ToString() ?? Environment.MachineName;

            await LogConsole.Instance.Log("Preferences.Load: {0}", _prefPath);
            var encrypted = await File.ReadAllTextAsync(_prefPath);
            var decrypted = await Decrypt(encrypted, _uid);
            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(decrypted));
            var prefs = await JsonSerializer.DeserializeAsync<Preferences>(stream, _options);
            if (prefs == null) return;
            Host = prefs.Host;
            Username = prefs.Username;
            Password = prefs.Password;
            Mac = prefs.Mac;
            UseSsh = prefs.UseSsh;
        }
        catch (Exception e)
        {
            await LogConsole.Instance.LogError("Preferences.Load: failed: {0}", e.Message);
        }
    }

    [System.Diagnostics.CodeAnalysis.UnconditionalSuppressMessage("Trimming", "IL2026")]
    public async Task Save()
    {
        try
        {
            var dir = Path.GetDirectoryName(_prefPath);
            if (!Directory.Exists(dir))
            {
                if (dir != null) Directory.CreateDirectory(dir);
            }

            var encrypted = await Encrypt(JsonSerializer.Serialize(this, _options), _uid);
            await File.WriteAllTextAsync(_prefPath, encrypted);
        }
        catch (Exception e)
        {
            await LogConsole.Instance.LogError("Preferences.Save: failed: {0}", e.Message);
        }
    }

    private static async Task<string> Encrypt(string plaintext, string password)
    {
        try
        {
            // convert the plaintext string to a byte array
            var plaintextBytes = Encoding.UTF8.GetBytes(plaintext);

            // generate a salt
            var salt = new byte[16];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            // derive the key using PBKDF2
            using var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 1000, HashAlgorithmName.SHA256);
            var key = pbkdf2.GetBytes(32); // 256-bit key
            var iv = pbkdf2.GetBytes(16); // 128-bit IV

            // use the derived key to encrypt the plaintext
            using var encryptor = Aes.Create();
            encryptor.Key = key;
            encryptor.IV = iv;
            encryptor.Padding = PaddingMode.PKCS7;

            using var ms = new MemoryStream();
            await using var cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(plaintextBytes, 0, plaintextBytes.Length);
            await cs.FlushFinalBlockAsync();

            // combine salt and encrypted data for storage
            var result = new byte[salt.Length + ms.Length];
            Buffer.BlockCopy(salt, 0, result, 0, salt.Length);
            Buffer.BlockCopy(ms.ToArray(), 0, result, salt.Length, (int)ms.Length);

            return Convert.ToBase64String(result);
        }
        catch (Exception e)
        {
            await LogConsole.Instance.LogError(e.Message);
            return string.Empty;
        }
    }

    private static async Task<string> Decrypt(string encrypted, string password)
    {
        try
        {
            // convert the encrypted string to a byte array
            var encryptedBytes = Convert.FromBase64String(encrypted);

            // extract the salt from the encrypted data
            var salt = new byte[16];
            Buffer.BlockCopy(encryptedBytes, 0, salt, 0, salt.Length);

            // derive the key using PBKDF2
            using var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 1000, HashAlgorithmName.SHA256);
            var key = pbkdf2.GetBytes(32); // 256-bit key
            var iv = pbkdf2.GetBytes(16); // 128-bit IV

            // use the derived key to decrypt the encrypted data
            using var encryptor = Aes.Create();
            encryptor.Key = key;
            encryptor.IV = iv;
            encryptor.Padding = PaddingMode.PKCS7;

            using var ms = new MemoryStream();
            await using var cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(encryptedBytes, salt.Length, encryptedBytes.Length - salt.Length);
            await cs.FlushFinalBlockAsync();

            return Encoding.UTF8.GetString(ms.ToArray());
        }
        catch (Exception e)
        {
            await LogConsole.Instance.Log(e.Message);
            return string.Empty;
        }
    }
}