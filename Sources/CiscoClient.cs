using System;
using System.Threading.Tasks;

namespace CMac2Port.Sources;

public class CiscoClient(CiscoClient.Proto protocol, string host, int port, string user, string password)
    : IDisposable
{
    public enum Proto
    {
        Ssh,
        Telnet
    }

    public string Name { get; set; } = string.Empty;
    public string Address { get; } = host;
    public int Port { get; } = port;
    public string Username { get; } = user;
    public string Password { get; } = password;

    public Proto Protocol { get; } = protocol;

    public virtual async Task<string?> Connect()
    {
        await Task.Delay(1);
        return null;
    }

    public virtual bool IsConnected => false;

    public virtual async Task<string?> Send(string cmd)
    {
        await Task.Delay(1);
        return null;
    }

    public virtual void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}