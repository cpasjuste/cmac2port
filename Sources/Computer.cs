﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CMac2Port.Sources;

public sealed class Computer : INotifyPropertyChanged
{
    private string _name = string.Empty;

    public string Name
    {
        get => _name;
        set => SetField(ref _name, value);
    }

    private string _mac = string.Empty;

    public string Mac
    {
        get => _mac;
        set => SetField(ref _mac, value);
    }

    private string _switch = string.Empty;

    public string Switch
    {
        get => _switch;
        set => SetField(ref _switch, value);
    }

    private string _switchIp = string.Empty;

    public string SwitchIp
    {
        get => _switchIp;
        set => SetField(ref _switchIp, value);
    }

    private string _interface = string.Empty;

    public string Interface
    {
        get => _interface;
        set => SetField(ref _interface, value);
    }

    private string _vlan = string.Empty;

    public string Vlan
    {
        get => _vlan;
        set => SetField(ref _vlan, value);
    }

    public void Update(Computer other)
    {
        if (Name != other.Name) Name = other.Name;
        if (Switch != other.Switch) Switch = other.Switch;
        if (SwitchIp != other.SwitchIp) SwitchIp = other.SwitchIp;
        if (Interface != other.Interface) Interface = other.Interface;
        if (Vlan != other.Vlan) Vlan = other.Vlan;
    }

    public override string ToString()
    {
        return "\"" + string.Join("\";\"", Name, Mac, Switch, SwitchIp, Interface, Vlan) + "\"";
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    private void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    private bool SetField<T>(ref T field, T value, [CallerMemberName] string? propertyName = null)
    {
        if (EqualityComparer<T>.Default.Equals(field, value)) return false;
        field = value;
        OnPropertyChanged(propertyName);
        return true;
    }
}