namespace CMac2Port.Sources;

public class CiscoInterface(string vlan, string mac, string port)
{
    public string Port { get; set; } = port;
    public string Vlan { get; set; } = vlan;
    public string Mac { get; set; } = mac;

    public override string ToString()
    {
        return "MAC: " + Mac + ", PORT: " + Port + ", VLAN: " + Vlan;
    }
}