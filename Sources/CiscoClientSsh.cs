using System;
using System.Text;
using System.Threading.Tasks;
using CMac2Port.Controls;
using Renci.SshNet;

namespace CMac2Port.Sources;

public class CiscoClientSsh(string host, int port, string user, string password)
    : CiscoClient(Proto.Ssh, host, port, user, password)
{
    private SshClient? _client;
    private ShellStream? _stream;

    public override async Task<string?> Connect()
    {
        await LogConsole.Instance.Log("checking if host is up ({0})...", Address);

        if (!Utility.IsUp(Address))
        {
            await LogConsole.Instance.LogError("could not connect to {0} on port {1}", Address, Port);
            return "host not reachable...";
        }

        try
        {
            await LogConsole.Instance.Log("connecting to device ({0})", Address);
            var info = new PasswordConnectionInfo(Address, Port, Username, Password)
            {
                Timeout = TimeSpan.FromSeconds(3),
                ChannelCloseTimeout = TimeSpan.FromSeconds(3),
                RetryAttempts = 1
            };
            _client = new SshClient(info);
        }
        catch (Exception e)
        {
            await LogConsole.Instance.LogError(e.Message);
            return e.Message;
        }

        // log-in
        await LogConsole.Instance.Log("logging in (user: {0}, password: xxx)...", Username);
        try
        {
            _client.Connect();
        }
        catch (Exception e)
        {
            await LogConsole.Instance.LogError(e.Message);
            await LogConsole.Instance.LogError("could not connect to {0} on port {1} ({2})", Address, Port, e.Message);
            return e.Message;
        }

        if (!_client.IsConnected)
        {
            await LogConsole.Instance.LogError("could not connect to {0} on port {1}", Address, Port);
            return "unknown error...";
        }

        // create a shell stream (cisco fix...)
        _stream = _client.CreateShellStream("xterm", 80, 600, 0, 0, 32 * 1024);

        // be sure we get full output
        await Send("terminal length 0");

        return null;
    }

    public override bool IsConnected => _client is { IsConnected: true };

    public override async Task<string?> Send(string cmd)
    {
        if (_client == null || _stream == null) return null;

        try
        {
            await _stream.WriteAsync(Encoding.ASCII.GetBytes(cmd + "\r\n"));
            await _stream.FlushAsync();

            // wait for response
            while (!_stream.DataAvailable) await Task.Delay(1);

            // read until second prompt read (1st = command (echo), 2nd = end)
            var promptDone = false;
            var reply = string.Empty;
            while (true)
            {
                var line = _stream.ReadLine() + "\r\n";
                if (line.Contains('#'))
                {
                    if (promptDone)
                    {
                        reply = reply.Trim();
                        break;
                    }

                    promptDone = true;
                }

                if (promptDone) reply += line;
            }

            // be sure reply is not empty
            if (string.IsNullOrEmpty(reply)) return null;

            // cleanup reply (remove command "echo")...
            var start = reply.IndexOf("\r\n", StringComparison.Ordinal) + 2;
            if (start > 0 && start < reply.Length) reply = reply.Substring(start, reply.Length - start);

            return reply.Trim();
        }
        catch (Exception e)
        {
            await LogConsole.Instance.Log(e.Message);
        }

        return null;
    }

    public override void Dispose()
    {
        _stream?.WriteLine("exit");
        _stream?.Flush();
        _client?.Disconnect();
        _stream?.Dispose();
        _client?.Dispose();
        GC.SuppressFinalize(this);
    }
}